FROM fedora:28
LABEL "name"="copr-builder"
RUN dnf -y install rpm-build git jq copr-cli cronie && \
    dnf -y install make gcc
RUN useradd --system --user-group fcron
RUN wget http://fcron.free.fr/archives/fcron-3.2.1.src.tar.gz -O /tmp/fcron.tgz && \
    mkdir /tmp/fcron && \
    tar -xf /tmp/fcron.tgz -C /tmp/fcron --strip-components=1
# `make install` will fail on systemd, but we don't care because the executables are already installed
RUN cd /tmp/fcron && \
     ./configure --prefix=/usr --sysconfdir=/etc --with-fcrondyn=no --with-sendmail=no --with-answer-all=no --with-boot-install=no && \
     make && \
     (make install || true)
RUN rm -rf /tmp/fcron /tmp/fcron.tgz
ADD crontab /crontab
RUN fcrontab /crontab
ADD runner .
ADD launcher .
CMD ["/launcher"]

